https://stackoverflow.com/questions/19632405/prolog-implement-and-2-or-2-nand-2-nor-2-xor-2

and(A,B) :- A,B.
or(A,B) :- A;B.
nand(A,B) :- not(and(A,B)).
nor(A,B) :- not(or(A,B)).
xor(A,B) :- or(A,B), nand(A,B).

Prolog Assignment
and: 	x, y.
or:		x; y.
not:	\+(x).
xor:	((x; y), \+((x, y))).



Question 1.

			https://stackoverflow.com/questions/19632405/prolog-implement-and-2-or-2-nand-2-nor-2-xor-2

	and: 	x, y.
	or:		x; y.
	not:	\+(x).
	xor:	((x; y), \+((x, y))).

It would be nice if the language returned false for a nonexistant predicates as 
this was the content of the assignment but had to add guard clauses around the 
predicates when testing if they were true, using current_predicate(#name#/0). 
For more complex rules there were simpler more global ways of achieving this 
functionality without wrapping each rule in a check, but for simple fact checks 
it seemed like there was no way to achieve this without wrapping each fact
in a predicate check. We could have just wrapped the non-existant  facts in a 
predicate check but this seemed like cheated the point of learning the language.

so 
	predicate exists: current_predicate(#name#/0)
was also required

Both the idealised version and the actual working code version are listed below.

1. sunny AND warm
	sunny, warm.
	current_predicate(sunny/0), current_predicate(warm/0).
	
2. sunny AND cold
	sunny, cold.
	current_predicate(sunny/0), current_predicate(cold/0).
	
3. sunny OR cold
	sunny, cold.
	current_predicate(sunny/0); current_predicate(cold/0).
	
4. (sunny OR cold) AND warm
	(sunny; cold), warm.
	(current_predicate(sunny/0); current_predicate(cold/0)), current_predicate(warm/0).
	
5. happy XOR sunny
	((happy; sunny), \+((happy, sunny))).
	((current_predicate(happy/0); current_predicate(sunny/0)), \+((current_predicate(happy/0), current_predicate(sunny/0)))).
	
6. warm XOR ( not happy)
	((warm; \+(happy)), \+((warm, \+(happy)))).
	((current_predicate(warm/0); \+(current_predicate(happy/0))), \+((current_predicate(warm/0), \+(current_predicate(happy/0))))).
	
7. early NAND happy
	\+((early, happy)).
	\+((current_predicate(early/0), current_predicate(happy/0))).
	
8. (late NOR (not early) ) AND (windy OR (not warm))
	(\+(late; \+(early)), (windy; \+(warm))).
	(\+((current_predicate(late/0); \+(current_predicate(early/0)))), (current_predicate(windy/0); \+(current_predicate(warm/0)))).

9. (cloudy AND windy) AND ( warm AND early)
	((cloudy, windy), (warm, early)).
	((current_predicate(cloudy/0), current_predicate(windy/0)), (current_predicate(warm/0), current_predicate(early/0))).

10. (cloudy AND windy) XOR (warm OR early) 
	(((cloudy, windy); (warm; early)), \+(((cloudy, windy), (warm; early)))).
	(((current_predicate(cloudy/0), current_predicate(windy/0)); (current_predicate(warm/0); current_predicate(early/0))), \+(((current_predicate(cloudy/0), current_predicate(windy/0)), (current_predicate(warm/0); current_predicate(early/0))))).


Question 2.
1. 
teaches(Teacher,Student):-
	instructs(Teacher,X),takes(Student,X).

2.
teaches(bob, X);

3.
teaches(X,mary).

4.
'teaches(ann, joe).' is false because ann does not teach joe. Joe is only in 
CT331 and the instrutor for that course is not ann.

5. 
classmates(Student1, Student2):-
	takes(Student1,X),takes(Student2,X).


Question 3.
1.

2.

3.

4.

5.








