/* IsElementInList(Element, List) */
isElementInList(X, [X|_]):-true.
isElementInList(X, [_|T]):-
    isElementInList(X, T).

/* mergeLists(List1,List2,Merged) */
mergeLists([], L, L).
mergeLists([X|List1], List2, [X|Merged]):-
    mergeLists(List1,List2,Merged).

/* reverseList(List,reversedList) */
reverseList([], []).
reverseList([X|List], ReversedList):-
    reverseList(List, L), mergeLists(L,[X], ReversedList).

/* insertElementIntoListEnd(El, List, NewList) */
insertElementIntoListEnd(El, List, NewList):-
    mergeLists(List,[El],NewList).
























